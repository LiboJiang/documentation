# Curation request YAML format

Each curation request email should be accompanied by an attached YAML file specifying details of the assembly files and the process used to create them. Having this information specified in a standard, machine-readable manner allows us to automate some aspects of ticket processing.

In the description below, mandatory fields are preceded by an asterisk. This asterisk shouldn't actually appear in the YAML!

The specimen name should, if possible, conform to ToLID format, eg qxSacCarc1. If that is not possible, then it's highly preferable for the sample name to start with the same first letter that would be used for the ToLID (eg "i" for insects). Even if the specimen name does not correspond to a ToLID, the specimen name should use only alphanumeric characters, avoiding other special characters (including ".", "_", and "-"). This is because this specimen name is used as the basis for a MySQL database name, and should therefore avoid characters that are not permitted in MySQL database names.

There should always be a location for an assembly, so at least one of the following fields should be filled in: `primary, maternal, paternal`.

Where a field is not applicable, that field should simply be omitted. In particular, where a particular assembly type doesn't exist (usually haplotigs or mito), the relevant field should simply be omitted. Do *not* provide the key for the field and leave it empty, or supply a value such as "N/A".

The `data_location` field pertains to all the assembly and data fields. The possible values are:

* `Sanger RW`: For files on Sanger drives in directories that GRIT have write access to, and to which we are expected to write temporary files. This is assumed to be the default, and will be used for Darwin projects.

* `Sanger RO`: For files on Sanger drives in directories to which GRIT have read-only access. This is likely to be the case for faculty projects.

* `S3`: For files on GenomeArk S3

* `FTP`: For files accessible via FTP

The `pipeline` field, whilst not mandatory, is strongly encouraged for any assembly that Sanger is planning to submit.

Note that multi-line strings, as typically found in the "stats" section, must be indented in a uniform fashion (typically two spaces). Failure to do this is a common cause of problems.

Read directory locations are not required for Sanger DToL assemblies, where these are found at a predictable location in the directory structure, but they are required for all other assemblies. Preferred file formats differ for the different file types as described below. Note that these formats are preferences, not requirements.

```
---
*species: Dugesia japonica
*specimen: hrDugJapo1
*projects: Other
release_to: FTP
release_ftp_url: http://data-deliver.novogene.com/listobjects/CP2018091214179%2FC101SC18050674%2FRSCA0001%2FP101SC18050674-01%2FP101SC18050674-01-F004%2F
data_location: CP2018091214179/C101SC18050674/RSCA0001/P101SC18050674-01/P101SC18050674-01-F004/
primary: None
haplotigs: None
mito:  None
maternal:  None
paternal:  None
mito_gb:  None
primary_contigs:  None
jira_queue:  None
agp: None
hic: None
pacbio_read_dir: CP2018091214179/C101SC18050674/RSCA0001/P101SC18050674-01/P101SC18050674-01-F004/01.genome_sequence/Pacbio_data/
10x_read_dir: CP2018091214179/C101SC18050674/RSCA0001/P101SC18050674-01/P101SC18050674-01-F004/01.genome_sequence/10X_data/
hic_read_dir: CP2018091214179/C101SC18050674/RSCA0001/P101SC18050674-01/P101SC18050674-01-F004/01.genome_sequence/Hic_data/
illumina_data: CP2018091214179/C101SC18050674/RSCA0001/P101SC18050674-01/P101SC18050674-01-F004/01.genome_sequence/Illumina_data/
pacbio_read_type: hifi
hic_kit: None
ploidy: diploid
karyotype: 2n=16
karyotype_source: None
pretext: None
hic_map_img: None
kmer_spectra_img: None
busco_lineage: None
busco_gene_set_species: None
pipeline: None
notes: None
stats: None

